import os
import fabtools
from fabric.api import *

from fabric.contrib.files import exists

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DOCS_DIR = os.path.join(PROJECT_DIR)


def project_user():
    username = env.project.username
    home = '/home/%s' % username
    with settings(user=env.project.name, password=env.project.password):
        fabtools.require.directory(home, owner=username, group=username,
                                   use_sudo=True)
        fabtools.require.directory('/var/log/%s' % username, owner=username,
                                   group=username, use_sudo=True)

def sudo_project(*args, **kwargs):
    kwargs.update({
        'user': env.project.username,
        'group': env.project.username
    })
    return sudo(*args, **kwargs)


def python():
    username = env.project.username
    home = '/home/%s' % username
    repo = env.project.src_repo
    path = '%s/%s' % (home, env.project.src_dir)

    # Clean old installation
    if fabtools.files.is_dir(path):
        with settings(user=env.project.name, password=env.project.password):
            sudo('rm -rf %s' % path)

    # Cloning:
    with cd(home):
        sudo_project('git clone -q {repo} {dest}'.format(repo=repo, dest=path))

    # Co needed branch+
    with cd(path):
        sudo_project('git checkout %s' % env.project.src_branch)

    with cd(path):
        sudo_project('git pull')

    # Creating venv
    venv = '%s/%s' % (home, env.project.venv)
    pip = '%s/bin/pip' % venv
    with cd(home):
        if not fabtools.files.is_dir(venv):
            with settings(user=env.project.name, password=env.project.password):
                sudo_project('virtualenv --python=python%s %s' % (env.python_version, venv))
                sudo_project('%s install --upgrade pip' % pip)

        reqs = '%s/%s' % (path, env.project.reqs)
        cache_dir = os.path.join(home, env.project.pip_cache)

        with settings(user=env.project.name, password=env.project.password):
            fabtools.require.directory(
                cache_dir,
                use_sudo=True,
                owner=env.project.username,
                group=env.project.username
            )

        sudo_project('{pip} install -r {reqs} --no-use-wheel '
                     '--cache-dir={cache}'.format(pip=pip, reqs=reqs,
                                                  cache=cache_dir))


def copy_file(src, dst):
    sudo('cp -f {src} {dst}'.format(src=src, dst=dst))


def restart_service(name):
    with settings(user=env.project.name, password=env.project.password):
        sudo('/etc/init.d/{} restart'.format(name))


def nginx():
    with settings(user=env.project.name, password=env.project.password):
        fabtools.require.deb.ppa('ppa:nginx/stable')
        fabtools.require.deb.package('nginx', update=True)

        copy_file(
            src=os.path.join(env.src_path, 'deploy_assets/nginx_tests.conf'),
            dst='/etc/nginx/sites-enabled/tests.conf')

        restart_service('nginx')


def supervisord():
    with settings(user=env.project.name, password=env.project.password):
        fabtools.require.directory('/etc/supervisor/conf.d/', owner='root', group='root', use_sudo=True)
        sudo('/etc/init.d/supervisor restart')

        config = os.path.join(
            env.project.home,
            env.project.src_web,
            'deploy_assets',
            'tests_supervisor.conf'
        )

        sup_conf = '/etc/supervisor/conf.d/%s.conf' % env.project.name
        sudo('rm -f %s' % sup_conf)
        sudo('ln -s {conf} {sup}'.format(conf=config, sup=sup_conf))


def restart_supervisor():
    with settings(user=env.project.name, password=env.project.password):
        fabtools.supervisor.update_config()
        restart_service('redis-server')
        fabtools.supervisor.restart_process('general')
        fabtools.supervisor.restart_process('selenium')

def copy_project(src_path):
    copied_src = os.path.join(env.project.home, 'web_new')
    with settings(hide('stdout'), warn_only=True, user=env.project.name, password=env.project.password):
        sudo('rm -rf %s' % copied_src)

    sudo_project('cp -R %s %s' % (src_path, copied_src))
    return copied_src


def copy_settings(src_path):
    with settings(user=env.project.name, password=env.project.password):
        fabtools.require.directory('/etc/uwsgi/sites/', owner='root', group='root', use_sudo=True)

        sudo('cp -f {src} {dst}'.format(
            src=src_path + '/deploy_assets/emperor.ini',
            dst='/etc/uwsgi/'
        ))

        sudo('cp -f {src} {dst}'.format(
            src=src_path + '/deploy_assets/uwsgi.ini',
            dst='/etc/uwsgi/sites/'
        ))

        sudo('cp -f {src} {dst}'.format(
            src=src_path + '/deploy_assets/uwsgi.service',
            dst='/etc/systemd/system/'
        ))

    run('cp -f {src} {dst}'.format(
        src=src_path + '/deploy_assets/settings_prod.py',
        dst=src_path + '/tests/tests/local_settings.py'
    ))


def manage(cmd):
    # src path should be set by caller
    with cd(env.src_path):
        cmd = '%s %s %s' % (
            env.project.python, env.project.manage, cmd)
        return sudo_project(cmd)


def make_static(src_path):
    with cd(src_path):
        manage('collectstatic --noinput --ignore=cache '
               '--ignore=upload --ignore=multiuploader_images')


def migrate_db2(src_path):
    with cd(src_path):
        manage('migrate')


def move_project(src_path):
    pjt = os.path.join(env.project.home, env.project.src_web)
    old = os.path.join(env.project.home, 'web_old')

    if fabtools.files.is_dir(pjt) and fabtools.files.is_dir(old):
        with settings(warn_only=True, user=env.project.name, password=env.project.password):
            sudo('rm -rf %s' % old)

    if fabtools.files.is_dir(pjt):
        with settings(warn_only=True, user=env.project.name, password=env.project.password):
            sudo('mv %s %s' % (pjt, old))

    with settings(warn_only=True, user=env.project.name, password=env.project.password):
        sudo('mv %s %s' % (src_path, pjt))

    return pjt, old


def clean_project(old):
    with settings(hide('stdout'), warn_only=True, user=env.project.name, password=env.project.password):
        sudo('rm -rf %s' % old)


def require_packages(requirements_path):
    with open(requirements_path, 'r') as req_file:
        packages = req_file.readlines()

    clean = lambda s: s.strip('\n')
    packages = map(clean, packages)

    print "Following packages required:", packages
    with settings(warn_only=True, user=env.project.name, password=env.project.password):
        fabtools.require.deb.packages(packages, update=True)


def postgresql():
    with settings(user=env.project.name, password=env.project.password):
        sudo('wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -')
        fabtools.require.deb.source('pgdg', 'http://apt.postgresql.org/pub/repos/apt/', 'precise-pgdg', 'main')
        fabtools.require.postgres.server(version='9.5')
        fabtools.require.postgres.user('django', 'z3zvmE2p6Tp8e2830B8OEvc1p')
        fabtools.require.postgres.database('tests_db', owner='django')
        sudo('update-rc.d postgresql enable')

def postgres():
    with settings(user=env.project.name, password=env.project.password):
        if not fabtools.postgres.user_exists('django'):
            fabtools.postgres.create_user('django', 'z3zvmE2p6Tp8e2830B8OEvc1p')

        if not fabtools.postgres.database_exists('tests_db'):
            fabtools.postgres.create_database('tests_db', 'django')

        if not fabtools.service.is_running('postgresql'):
            fabtools.service.start('postgresql')


def restart_celery():
    config = os.path.join(
        env.project.home,
        env.project.src_web,
        'deploy_assets',
        'tests_supervisor.conf'
    )
    with settings(warn_only=True, user=env.project.name, password=env.project.password):
        # Ensure that our config is included:
        sup_conf = '/etc/supervisor.d/%s.conf' % env.project.name
        sudo('rm -f %s' % sup_conf)
        sudo('ln -s {conf} {sup}'.format(sup=sup_conf, conf=config))

        fabtools.supervisor.update_config()

        restart_service('redis-server')
        # if fabtools.supervisor.process_status('redis_celery') != 'RUNNING':
        #     fabtools.supervisor.start_process('redis_celery')

        fabtools.supervisor.restart_process('general')

        sudo('update-rc.d supervisord defaults')


@task
def full():
    src_path = os.path.join(env.project.home, env.project.src_dir)
    env.src_path = src_path

    require_packages(os.path.join(
        os.path.dirname(DOCS_DIR), 'deb_requirements.txt'))

    postgres()

    project_user()
    python()
    nginx()
    supervisord()

    src_path = env.src_path = copy_project(src_path)
    env.project.manage = os.path.join(src_path, 'tests', 'manage.py')

    copy_settings(src_path)

    make_static(src_path)
    migrate_db2(src_path)

    pjt, old = move_project(src_path)

    clean_project(old)
    with settings(user=env.project.name, password=env.project.password):
        if fabtools.systemd.is_running:
            fabtools.systemd.stop_and_disable('uwsgi')
        fabtools.systemd.start_and_enable('uwsgi')

    restart_celery()
    restart_supervisor()
