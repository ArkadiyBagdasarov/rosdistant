API_CHECK_CSRF = False
DEBUG = False

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tests_db',
        'USER': 'django',
        'PASSWORD': 'z3zvmE2p6Tp8e2830B8OEvc1p',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_EXPOSE_HEADERS = (
    'Set-Cookie',
)
CORS_ORIGIN_WHITELIST = (
    "localhost:8000",
    "127.0.0.1:8000"
)

# Celery
BROKER_URL = 'redis://:z3zvmE2p6Tp8e2830B8OEvc1p@127.0.0.1:6379/2'
CELERY_RESULT_BACKEND = 'redis://:z3zvmE2p6Tp8e2830B8OEvc1p@127.0.0.1:6379/1'
