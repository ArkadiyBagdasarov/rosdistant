import json

from selenium import webdriver

# from login import MainTest
from selenium.common.exceptions import NoSuchElementException

from selenium.webdriver.common.keys import Keys


class MainTest:
    def __init__(self, login, password, driver):
        self.login = login
        self.password = password
        self.driver = driver

    def logining(self):
        driver = self.driver
        driver.get("http://edu.rosdistant.ru")
        login = driver.find_element_by_id("login")
        login.send_keys(self.login)

        password = driver.find_element_by_id('password')
        password.send_keys(self.password)

        submit = driver.find_element_by_id('submit')
        submit.click()
        driver.implicitly_wait(5)

    def select_course(self):
        driver = self.driver
        self.course = 'Cost-менеджмент'
        course = driver.find_element_by_link_text(self.course)
        course.click()
        driver.implicitly_wait(5)

    def make_all(self):
        driver = self.driver
        self.current_url = driver.current_url
        self.current_test_list = [i.text for i in driver.find_elements_by_class_name('activityinstance') if
                                  i.text.startswith('И')]
        for i in self.current_test_list:
            self.make_one(i)
        driver.get(self.current_url)

    def make_one(self, test):
        self.title = self.course
        self.driver.find_element_by_link_text(test).click()
        self.driver.implicitly_wait(5)
        self.get_questions_and_answers()

    def get_questions_and_answers(self):
        self.go_in_test()

    def go_in_test(self):
        driver = self.driver
        singlebutton = driver.find_element_by_class_name('singlebutton')
        button = singlebutton.find_element_by_tag_name('input')
        button.click()
        driver.implicitly_wait(5)
        self.mark_right_answer()

    def mark_right_answer(self):
        driver = self.driver
        self.answers = []
        with open(self.title + '/main', 'r') as f:
            for s in f.readlines():
                string = json.loads(s)
                self.answers.append(string)

        for i in self.answers:
            try:
                right_answer = [self.get_right_answer()]
            except NoSuchElementException:
                break
            if right_answer[0]:
                try:
                    answer = driver.find_element_by_class_name('answer')
                except NoSuchElementException:
                    driver.find_element_by_class_name('submitbtns').find_element_by_tag_name('input').click()
                    driver.implicitly_wait(5)
                    continue
                labels = answer.find_elements_by_tag_name('label')
                for label in labels:
                    if label.text in right_answer:
                        label.click()
                        driver.implicitly_wait(5)
                    else:
                        qs = [i.strip() for i in right_answer[0].split(',')]
                        if label.text in qs:
                            label.click()
                            driver.implicitly_wait(5)

            try:
                driver.find_element_by_class_name('submitbtns').find_element_by_tag_name('input').click()
                driver.implicitly_wait(5)
            except NoSuchElementException:
                break
        driver.get(self.current_url)


        # get_result_form = driver.find_element_by_xpath('//*[@id="region-main"]/div/div[3]/div/div/form')
        # get_result_button = get_result_form.find_element_by_tag_name('input')
        # get_result_button.click()
        # driver.implicitly_wait(5)
        # confirm = driver.find_element_by_class_name('confirmation-buttons').find_element_by_tag_name('input')
        # confirm.click()
        # driver.implicitly_wait(5)

    def get_right_answer(self):
        driver = self.driver
        qtext = driver.find_element_by_class_name('qtext')
        try:
            question_text = qtext.find_element_by_tag_name('p')
        except NoSuchElementException:
            question_text = qtext
        q_text = question_text.text
        r_answer = None
        for answer in self.answers:
            if answer['q'] == q_text:
                r_answer = answer['a']
                break
        return r_answer

    def close(self):
        self.driver.close()


driver_main = webdriver.Chrome()
main_test = MainTest('9432', 'eujnoysw', driver_main)
main_test.logining()
main_test.select_course()
main_test.make_all()
