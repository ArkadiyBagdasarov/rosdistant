from fabric.api import *

import deploy.fabric.setup as setup

from fabric.utils import _AttributeDict

import os

env.project = _AttributeDict({
    'name': 'desktop',
    'username': 'desktop',
    'src_repo': 'git@bitbucket.org:ArkadiyBagdasarov/rosdistant.git',
    'reqs': 'requirements.txt',
    'src_dir': 'tmp_django',
    'src_web': 'tests_backend',
    'src_branch': 'master',
    'pip_cache': '.pip-cache',
    'venv': 'venv',
    'password': 'arkasha13'
})

env.project.home = os.path.join('/home', env.project.username)
env.project.pip = os.path.join(env.project.home, env.project.venv,
                               'bin', 'pip')
env.project.python = os.path.join(env.project.home, env.project.venv,
                                  'bin', 'python')


def co_branch():
    local('git checkout {branch}'.format(branch=env.project.src_branch))


def push():
    local('git push origin {branch}'.format(branch=env.project.src_branch))


@task
def prod():
    env.type = 'prod'
    env.python_version = '3.5'
    env.hosts = [
        'desktop@192.168.0.100'
    ]
    # co_branch()
    # push()


# Deploy tasks
@task
def full_deploy():
    push()
    setup.full()


# execute(prod)
# execute(full_deploy)

try:
    from fabfile_local import *
except ImportError:
    pass

try:
    from fabfile_local import modify

    modify(globals())
except ImportError:
    pass
