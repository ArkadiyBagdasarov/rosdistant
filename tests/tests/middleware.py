import logging

from django.db import connections
from django.template import Template, Context
from django.conf import settings
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

from account.utils import get_user_account


class UpdateLastActivityMiddleware(MiddlewareMixin):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user.is_authenticated():
            return

        account = get_user_account(request.user)

        if account is not None:
            account.last_activity_time = timezone.now()
            account.save(update_fields=['last_activity_time'])


class SQLLogMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

        if settings.DEBUG:
            for connection_name in connections:
                logging.info('[SQLLogMiddleware] conn_name = %s', connection_name)
                connection = connections[connection_name]
                logging.info('[SQLLogMiddleware] connection.queries = %s', connection.queries)
                if connection.queries:
                    logging.info('[SQLLogMiddleware] in')
                    time = sum([float(q['time']) for q in connection.queries])
                    header_t = Template("{{name}}: {{count}} quer{{count|pluralize:\"y,ies\"}} in {{time}} seconds")
                    logging.info(header_t.render(Context({
                        'name': connection_name,
                        'sqllog': connection.queries,
                        'count': len(connection.queries),
                        'time': time
                    })))
                    t = Template("{% for sql in sqllog %}[{{forloop.counter}}] {{sql.time}}s: {{sql.sql|safe}}{% if "
                                 "not forloop.last %}\n\n{% endif %}{% endfor %}")
                    logging.info(t.render(Context({'sqllog': connection.queries})))

        return response
