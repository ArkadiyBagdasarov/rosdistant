from decimal import Decimal
from django.db import models

from account.models import Account


class Wallet(models.Model):
    account = models.OneToOneField(Account)
    value = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.0'))

    def __str__(self):
        return 'wallet for: {}'.format(self.account.user.email)

    def process_payment(self, value):
        self.value -= value
        self.save(update_fields=['value'])
