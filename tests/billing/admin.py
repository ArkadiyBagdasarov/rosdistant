from django.contrib import admin

from billing.models import Wallet

admin.site.register(Wallet)
