function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function predmet(name, main_div){
	return '<div class="panel-heading">'+
	'<a class="panel-title" data-toggle="collapse" data-parent="#panel-805964" href="#'+main_div+'">'+ name +'</a></div>'
}

function tests_tead(main_div, id_tbody){
	return '<div id="'+main_div+'" class="panel-collapse collapse">'+
				'<div class="panel-body">'+
					'<table class="table table-condensed table-bordered">'+
						'<thead>'+
							'<tr>'+
								'<th>'+
								'</th>'+
								'<th>'+
									'ID'+
								'</th>'+
								'<th>'+
									'Название тест'+
								'</th>'+
								'<th>'+
									'Текущая оценка'+
								'</th>'+
								'<th>'+
									'Статус'+
								'</th>'+
							'</tr>'+
						'</thead>'+
						'<tbody id="'+id_tbody+'">'
}

function test_body(obj){
	var color;
	if(obj.status == 'in progress'){
		color = 'bg-danger';
	}
	else if(obj.status == 'end'){
		color = 'bg-success';
	}
	return '<tr class="'+color+'">'+
		'<td width="10%">'+
		'<label><input type="checkbox"/>НАЖМИ</label>'+
		'</td>'+
		'<td class="id_test">'+
			obj.id+
		'</td>'+
		'<td>'+
			obj.name+
		'</td>'+
		'<td>'+
			obj.rating+
		'</td>'+
		'<td>'+
			obj.status+
		'</td>'+
	'</tr>'
}

function generate_tests(obj){
	tests_var = $("#12331");
	tests_var.empty();
	Object.keys(obj).map(function(key, index) {
		tests_var.append(predmet(key, 'main_div_'+index));
		tests_var.last().append(tests_tead('main_div_'+index, 'tbody_id_'+index));
		obj[key].forEach(function(item, i, arr) {
			$('#tbody_id_'+index).last().append(test_body(item));
		});
		tests_var.last().append('</tbody></table></div>');
	});
}

function get_cabinet() {
	$.get("api/v1/cabinet/", function(data) {
		document.getElementById('rosdistant_login').innerHTML = data.login;
		document.getElementById('rosdistant_password').innerHTML = data.password;
	}).done(function () {
		get_tests();
    });
}

var tests = null;

function get_tests(){
	$.ajax({
		'type': "GET",
		'dataType': 'json',
		'url': "api/v1/cabinet/test/",
		'success': function (obj) {
			generate_tests(obj);
			tests = obj;
		}
	});
}

var new_data_rosdistant = $('#new-data-rosdistant');

new_data_rosdistant.click(function () {
	test();
	get_tests();
});

function test() {
	new_login = $('#input-new-login').val();
	new_password = $('#input-new-password').val();
	if(!new_login || !new_password){
		return
	}

	var csrftoken = getCookie('csrftoken');

	$.ajax({
		type: "POST",
		dataType: 'json',
		url: "api/v1/cabinet/",
		headers: { 'X-CSRFToken': csrftoken},
		data: {login: new_login, password: new_password},
		success: function (obj) {
			get_cabinet();
			new_data_rosdistant.addClass('btn-success')
		},
		error: function (obj) {
			var obj = jQuery.parseJSON(obj.responseText);
			new_data_rosdistant.addClass('btn-danger');
            alert(obj.errors.login);
        }
	});
}

function update_cabinet() {
	$.get("api/v1/cabinet/update/", function(data) {
		var timerId = setInterval(function() {
			get_tests();
			if (Object.keys(tests).length != 0 && tests.constructor === Object){
				clearInterval(timerId);
			}
		}, 5000);
	});
}

var new_update_cabinet = $('#update-cabinet');

new_update_cabinet.click(function () {
	update_cabinet()
});

function get_test_ids() {
	var tests_id = [];
	tests_block = $('#12331');
	tests = tests_block.find('input:checked').parents('tr').find('.id_test');
	tests.each(function(test) {
		tests_id.push(Number($(this).text()));
	});
	return tests_id
}

var done_select_test = $('#done_select_test');
done_select_test.click(function () {
	var test_ids = get_test_ids();
	test_ids.forEach(function (test_id) {

		$.post("api/v1/cabinet/test/" + test_id + "/")
		.done(function(data) {
			console.log('login', data.success);
			get_tests();
		})
		.error(function (obj) {
			var obj = jQuery.parseJSON(obj.responseText);
            alert(obj.errors);
        });
    })
});

get_cabinet();