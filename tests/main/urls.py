from django.conf.urls import url

from main.views import main, account, registration

urlpatterns = [
      url(r'^settings/$', main.SettingsView.as_view()),

      url(r'^accounts/register/$', registration.RegisterUserView.as_view()),
      url(r'^accounts/login/$', registration.LoginView.as_view()),
      url(r'^accounts/logout/$', registration.LogoutView.as_view()),
      url(r'^accounts/view/$', account.AccountView.as_view()),
      url(r'^accounts/edit/$', account.AccountEditView.as_view()),

      url(r'^cabinet/$', main.CabinetView.as_view()),
      url(r'^cabinet/update/$', main.UpdateCabinetView.as_view()),
      # url(r'^cabinet/payment/$', main.UpdateCabinetView.as_view()),
      url(r'^cabinet/test/$', main.TestListView.as_view()),
      url(r'^cabinet/test/(?P<pk>[0-9]+)/$', main.TestView.as_view()),
]
