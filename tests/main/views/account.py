from django.db import transaction
from django.contrib.auth import logout

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from account.utils import get_user_account
from main.mixin import AuthenticatedMixin

from main.serializers.account import AccountSerializer


class AccountView(AuthenticatedMixin, APIView):
    account_serializer = AccountSerializer

    def get(self, request, format=None):
        """
        Метод получения информации об аккаунте
        """
        account = get_user_account(request.user)
        if not account:
            return Response(dict(user_id=request.user.id,
                                 user_email=request.user.email))
        else:
            serializer_cls = self.account_serializer(account)

        response = serializer_cls.data
        return Response(response)


class AccountEditView(AuthenticatedMixin, APIView):
    serializer_class = AccountSerializer

    def post(self, request, format=None):
        """
        Метод изменения данных аккаунта.
        ---
        serializer: main.serializers.account.AccountSerializer
        """
        account = get_user_account(request.user)

        serializer = self.serializer_class(account, data=request.data)

        if not serializer.is_valid():
            return Response({'success': False, 'errors': serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)

        with transaction.atomic():
            serializer.save()

        if serializer.email_changed:
            logout(request)

        return Response({'success': True})
