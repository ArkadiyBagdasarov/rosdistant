from collections import defaultdict

from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Price, Cabinet
from main.serializers import serializers
from main.decorators import get_account, get_cabinet, check_status_cabinet
from main.mixin import AuthenticatedMixin
from main.tasks import done_test


class IndexView(TemplateView):
    template_name = "main/src/index.html"


class SettingsView(AuthenticatedMixin, APIView):
    def get(self, request):
        """
        Метод получения настроек
        ---
        """
        resp = {
            'price': Price.objects.values('name', 'value')
        }

        return Response(resp)


class CabinetView(APIView):
    """
    Метод получения/создания/изменения настроек кабинета
    ---
    POST:
        serializer: main.serializers.serializers.CabinetSerializer
    PUT:
        serializer: main.serializers.serializers.CabinetSerializer
    """
    serializer = serializers.CabinetSerializer

    @get_account
    @get_cabinet
    def get(self, request, account, cabinet):

        serializer = self.serializer(cabinet)
        return Response(serializer.data)

    @get_account
    def post(self, request, account):
        # if hasattr(account.user, 'cabinet'):
        #     return Response({'success': False, 'errors': ['уже есть кабинет']},
        #                     status=status.HTTP_400_BAD_REQUEST)

        serializer = self.serializer(
            data=request.data,
            context={'account': account}
        )

        try:
            cabinet = Cabinet.objects.get(login=serializer.initial_data['login'])
        except Cabinet.DoesNotExist:
            pass
        else:
            Cabinet.objects.filter(account=account).update(account=None)
            cabinet.account = account
            cabinet.save(update_fields=['account'])
            return Response(self.serializer(cabinet).data)

        if not serializer.is_valid():
            return Response({'success': False, 'errors': serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @get_account
    @get_cabinet
    @check_status_cabinet
    def put(self, request, account, cabinet):

        serializer = self.serializer(
            data=request.data,
            instance=cabinet
        )

        if not serializer.is_valid():
            return Response({'success': False, 'errors': serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(serializer.data)


class UpdateCabinetView(AuthenticatedMixin, APIView):
    """
    Метод обновления кабинета
    ---
    """
    @get_account
    @get_cabinet
    @check_status_cabinet
    def get(self, request, account, cabinet):

        cabinet.get_data()
        return Response({'success': True})


class CabinetPaymentView(AuthenticatedMixin, APIView):

    """
    Метод пополнения кабинета
    ---
    """
    serializer = serializers.TestSerializer

    @get_account
    @get_cabinet
    @check_status_cabinet
    def post(self, request, account, cabinet):

        return Response({'success': True})


class TestListView(AuthenticatedMixin, APIView):
    """
    Метод получения всех тестов
    ---
    """
    serializer = serializers.TestSerializer

    @get_account
    @get_cabinet
    def get(self, request, account, cabinet):
        if not cabinet.tests.count():
            return Response({})
        return Response(self._get_tests(cabinet))

    def _get_tests(self, cabinet):
        tests = defaultdict(list)
        test_obj = cabinet.tests.order_by('name').values('theme__name', 'name', 'rating', 'status', 'id')

        for test in test_obj:
            tests[test['theme__name']].append(test)
        return tests


class TestView(AuthenticatedMixin, APIView):
    """
    Метод получения одного теста/пометки теста на выполнение
    ---
    """
    serializer = serializers.TestSerializer

    @get_account
    @get_cabinet
    def get(self, request, account, cabinet, pk):
        test = cabinet.tests.filter(id=pk).first()

        if not test:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer(test)
        return Response(serializer.data)

    @get_account
    @get_cabinet
    @check_status_cabinet
    def post(self, request, account, cabinet, pk):
        test = cabinet.tests.filter(id=pk).first()

        if not test:
            return Response(status=status.HTTP_404_NOT_FOUND)

        available = cabinet.available_tests()

        if not available:
            return Response({'success': False, 'errors': ['недостаточно попыток выполнения']},
                            status=status.HTTP_400_BAD_REQUEST)

        cabinet.remove_one_count_test()
        done_test.delay(test.id)

        return Response({'success': True})
