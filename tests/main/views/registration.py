from django.contrib.auth import authenticate, login, logout
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from main.mixin import NotAuthenticatedMixin, AuthenticatedMixin
from main.serializers.account import RegisterSerializer, LoginSerializer


class RegisterUserView(NotAuthenticatedMixin, APIView):
    serializer_class = RegisterSerializer

    def post(self, request, format=None):
        """
        Метод регистрации пользователя.
        ---
        serializer: main.serializers.account.RegisterSerializer

        parameters:
            - name: email
              description: Email пользователя
            - name: phone
              description: Телефон в формате 79999999999
            - name: password
              description: Пароль пользователя

        responseMessages:
            - code: 400
              message: Не удалось создать пользователя
        """
        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return Response({'success': False, 'errors': serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)

        password = serializer.validated_data['password']

        user = serializer.save()

        self._authenticate_user(user, password)

        return Response({'success': True}, status=status.HTTP_201_CREATED)

    def _authenticate_user(self, user, password):
        auth_user = authenticate(username=user.username, password=password)
        login(self.request, auth_user)


class LoginView(NotAuthenticatedMixin, APIView):
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        """
        Метод авторизации пользователя.
        ---
        type:
          success:
            required: true
            type: boolean
          error:
            required: false
            type: string

        serializer: main.serializers.account.LoginSerializer

        parameters:
            - name: login
              description: Email пользователя
            - name: password
              description: Пароль пользователя
            - name: remember
              description: Запомнить пользователя?
              type: boolean

        responseMessages:
            - code: 400
              message: Не удалось авторизировать пользователя
        """

        def error_resp(err):
            return Response({
                'success': False,
                'errors': err,
            }, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid():
            return error_resp(serializer.errors)

        user = authenticate(
            username=serializer.validated_data['login'].lower(),
            password=serializer.validated_data['password']
        )

        if not user:
            return error_resp({'fields': ['Неверный логин или пароль']})

        if not user.is_active:
            return error_resp({'status': ['Пользователь неактивен']})

        login(request, user)

        if serializer.validated_data['remember']:
            request.session.set_expiry(60 * 60 * 24 * 7 * 3)
        else:
            request.session.set_expiry(0)

        return Response({'success': True})


class LogoutView(AuthenticatedMixin, APIView):
    def post(self, request, format=None):
        """
        Метод разавторизации пользователя.
        """
        logout(request)
        return Response({'success': True})
