def delete_another_cabinets(login, cabinet_id):
    from main.models import Cabinet

    cabinets = (
        Cabinet.objects
        .filter(login=login)
        .exclude(id=cabinet_id)
    )
    if cabinets:
        cabinets.delete()
