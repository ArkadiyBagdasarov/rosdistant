from django.db import models
from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from account.models import Account
from main.tasks import get_data_cabinet


class Cabinet(models.Model):
    STATUS_TEST = (
        ('not updated', 'не проверен'),
        ('updated', 'проверяется'),
        ('ok', 'проверен')
    )
    account = models.ForeignKey(Account, null=True)
    login = models.CharField('логин от росдистант', max_length=100, null=True)
    password = models.CharField('пароль от росдистант', max_length=100, null=True)
    date_create = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        'статус', null=True,
        blank=True, max_length=100,
        choices=STATUS_TEST, default=STATUS_TEST[0][0]
    )
    last_update = models.DateTimeField('последнее обновление', null=True)
    count_tests = models.IntegerField('доступное кол-во тестов для выполнения', default=1)

    def __str__(self):
        return 'id: {}, login: {}'.format(str(self.id), self.login)

    def get_data(self):
        get_data_cabinet.delay(self.id)

    def set_in_progress(self):
        self.status = self.STATUS_TEST[1][0]
        self.save(update_fields=['status'])

    def set_active(self):
        self.status = self.STATUS_TEST[2][0]
        self.save(update_fields=['status'])

    def set_last_update(self):
        self.last_update = timezone.now()
        self.save(update_fields=['last_update'])

    def payment_tests(self, count_tests, name_service):
        with transaction.atomic():
            service = Price.objects.filter(name=name_service).first()

            if not service:
                return True, 'нет такой услуги'

            wallet = self.account.wallet
            value_off = int(count_tests * service.value)

            if value_off > wallet.value:
                return True, 'недостаточно средств'

            wallet.process_payment(value_off)
            self.set_count_tests(count_tests)

            return False, 'done'

    def set_count_tests(self, count_tests):
        self.count_tests += count_tests
        self.save(update_fields=['count_tests'])

    def available_tests(self):
        return bool(self.count_tests)

    def remove_one_count_test(self):
        self.count_tests -= 1
        self.save(update_fields=['count_tests'])


class NameTheme(models.Model):
    name = models.CharField('название темы', max_length=200)

    def __str__(self):
        return 'name: {}'.format(self.name)


class NameTest(models.Model):
    STATUS_TEST = (
        ('new', 'не выполнен'),
        ('in progress', 'выполняется'),
        ('end', 'завершен')
    )
    theme = models.ForeignKey(NameTheme, related_name='tests')
    cabinet = models.ForeignKey(Cabinet, related_name='tests')
    name = models.CharField('название теста', max_length=100)
    rating = models.CharField('кол-во баллов', max_length=10)
    date_create = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        'статус', null=True,
        blank=True, max_length=100,
        choices=STATUS_TEST, default=STATUS_TEST[0][0]
    )

    class Meta:
        ordering = ['theme__name', 'name']

    def __str__(self):
        return (
            'user: {}, theme: {}, name: {}'
            .format(self.cabinet.login, self.theme.name, self.name)
        )

    def set_in_progress(self):
        self.status = self.STATUS_TEST[1][0]
        self.save(update_fields=['status'])

    def set_end(self):
        self.status = self.STATUS_TEST[2][0]
        self.save(update_fields=['status'])


class Price(models.Model):
    name = models.CharField('название услуги', max_length=50)
    value = models.IntegerField('стоимость услуги', default=0)

    def __str__(self):
        return self.name
