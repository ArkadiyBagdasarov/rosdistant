from rest_framework import status
from rest_framework.response import Response

from account.utils import get_user_account
from main.models import Cabinet


def get_account(function):
    def wrap(request, *args, **kwargs):
        account = get_user_account(request.request.user)
        if not account:
            return Response({
                'success': False,
                'errors': ['Необходима авторизация'],
            }, status=status.HTTP_400_BAD_REQUEST)

        kwargs['account'] = account

        return function(request, *args, **kwargs)
    return wrap


def get_cabinet(function):
    def wrap(request, *args, **kwargs):
        try:
            kwargs['cabinet'] = kwargs['account'].cabinet_set.last()
        except AttributeError:
            return Response({'success': False, 'errors': ['сначала создайте кабинет']},
                            status=status.HTTP_400_BAD_REQUEST)

        return function(request, *args, **kwargs)
    return wrap


def check_status_cabinet(function):
    def wrap(request, *args, **kwargs):
        if kwargs['cabinet'].status == Cabinet.STATUS_TEST[1][0]:
            return Response({'success': False, 'errors': ['кабинет обновляется']},
                            status=status.HTTP_400_BAD_REQUEST)

        return function(request, *args, **kwargs)
    return wrap