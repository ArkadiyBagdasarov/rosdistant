import logging
from celery import shared_task

from main.utils import delete_another_cabinets
from rosdistant.get_data_cabinet_selenium import get_data_selenium
from rosdistant.rosdistant import DoTest

logger = logging.getLogger()


@shared_task(name='get_data_cabinet')
def get_data_cabinet(cabinet_id):
    from main.models import Cabinet
    from main.models import NameTheme
    from main.models import NameTest

    cabinet = Cabinet.objects.get(id=cabinet_id)
    cabinet.set_in_progress()
    logger.info('cabinet id: {}, set in progress'.format(cabinet_id))

    test_obj = get_data_selenium(cabinet.login, cabinet.password)
    for theme_key in test_obj:
        logger.info('iter theme with name: {}'.format(theme_key))

        theme, create = NameTheme.objects.get_or_create(name=theme_key)
        for test_key in test_obj[theme_key]:
            test, create = NameTest.objects.get_or_create(
                theme=theme,
                cabinet=cabinet,
                name=test_key
            )
            test.rating = test_obj[theme_key][test_key]
            test.save(update_fields=['rating'])
    logger.info('save all tests')

    cabinet.set_active()
    cabinet.set_last_update()
    logger.info('cabinet update and set active')

    delete_another_cabinets(cabinet.login, cabinet.id)
    logger.info('delete another cabinets wuth login: {} was deleted'.format(cabinet.login))


@shared_task(name='done_test')
def done_test(test_id):
    from main.models import NameTest

    test = NameTest.objects.get(id=test_id)
    data = {
        'login': test.cabinet.login,
        'password': test.cabinet.password,
        'course': test.theme.name,
        'tests': [test.name]
    }
    logger.info('done test: {} start make'.format(test_id))
    test.set_in_progress()
    with DoTest(**data) as do_test:
        do_test.start()
    test.set_end()
    logger.info('done test: {} end make'.format(test_id))
