from django.contrib import admin

from main import models

admin.site.register(models.Cabinet)
admin.site.register(models.NameTheme)
admin.site.register(models.NameTest)
admin.site.register(models.Price)
