from rest_framework import serializers
from rest_framework.serializers import ValidationError

from main import models


def unique_login(value):
    cabinet = models.Cabinet.objects.filter(
        login=value,
        status=models.Cabinet.STATUS_TEST[2][0]
    ).exists()

    if cabinet:
        raise ValidationError("кабинет с таким логином уже есть")


class CabinetSerializer(serializers.ModelSerializer):
    login = serializers.CharField(validators=[unique_login], required=False)
    password = serializers.CharField(required=False)
    status = serializers.CharField(read_only=True)

    class Meta:
        model = models.Cabinet
        fields = ('login', 'password', 'status', 'id')

    def create(self, validated_data):
        models.Cabinet.objects.filter(account=self.context['account']).update(account=None)
        validated_data['account'] = self.context['account']
        return models.Cabinet.objects.create(**validated_data)

    def update(self, instance, validated_data):
        for field in validated_data:
            setattr(instance, field, validated_data[field])
        instance.save()
        return instance


class TestSerializer(serializers.ModelSerializer):
    theme = serializers.CharField(source='theme.name')

    class Meta:
        model = models.NameTest
        fields = ('theme', 'name', 'rating', 'status', 'id')
