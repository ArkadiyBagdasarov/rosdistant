from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.serializers import ValidationError
from rest_framework.validators import UniqueValidator

from account.models import Account
from account.utils import change_user_email, change_account_phone


class LowercaseEmailField(serializers.EmailField):
    def to_internal_value(self, data):
        val = super().to_internal_value(data)
        return val.lower()


rus_name_regex = r'^[а-яА-ЯёЁa-zA-Z ]{2,}$'


def unique_phone(value):
    account_count = Account.objects.filter(
        phone=value
    ).count()

    if account_count != 0:
        raise ValidationError('Такой телефон уже зарегистрирован')


def unique_email(value):
    c = User.objects.filter(email=value).count()
    if c != 0:
        raise ValidationError(
            'Данный email уже зарегистрирован в системе')


class RegisterSerializer(serializers.Serializer):
    email = LowercaseEmailField(max_length=128, validators=[
        UniqueValidator(
            queryset=User.objects.all(),
            message='Этот адрес уже занят'
        )
    ])
    phone = serializers.RegexField(regex=r'^7\d{10}$', validators=[unique_phone])
    password = serializers.CharField(min_length=6, max_length=32)

    def validate(self, data):
        phone = data['phone']
        import re
        if not re.match(r'^7\d{10}$', phone):
            raise serializers.ValidationError({
                'phone': 'Укажите корректный телефон'})
        return data

    def create(self, validated_data):
        return Account.objects.create_account_user(
            email=validated_data['email'],
            password=validated_data['password'],
            phone=validated_data['phone'],
        )


class LoginSerializer(serializers.Serializer):
    login = serializers.CharField(max_length=128)
    password = serializers.CharField(max_length=32)
    remember = serializers.BooleanField()


class AccountSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    email = serializers.EmailField(max_length=127, validators=[unique_email],
                                   required=False, source='user.email')
    first_name = serializers.RegexField(regex=rus_name_regex, max_length=128,
                                        required=False)
    last_name = serializers.RegexField(regex=rus_name_regex, max_length=128,
                                       required=False)
    middle_name = serializers.CharField(
        required=False,
        max_length=128,
        allow_blank=True
    )
    phone = serializers.RegexField(regex=r'^7\d{10}$', required=False,
                                   validators=[unique_phone])

    update_fields = (
        'first_name', 'last_name', 'middle_name'
    )

    name_fields = ('first_name', 'last_name', 'middle_name')

    def validate(self, data):
        phone = data.get('phone')
        if phone:
            import re
            if not re.match(r'^7\d{10}$', phone):
                raise serializers.ValidationError({
                    'phone': 'Укажите корректный телефон'})
        return data

    def update(self, instance, validated_data):
        for field in self.name_fields:
            v = validated_data.get(field)
            if v:
                validated_data[field] = v.capitalize()

        for field in self.update_fields:
            v = validated_data.get(field)
            if v is not None:
                setattr(instance, field, v)

        instance.save()

        try:
            email = validated_data['user']['email']
        except KeyError:
            email = None

        if email:
            change_user_email(instance.user, email)
            self.email_changed = True
        else:
            self.email_changed = False

        if validated_data.get('phone'):
            change_account_phone(instance, validated_data['phone'])

        return instance
