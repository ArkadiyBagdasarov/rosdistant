import abc


class LoginAbs(metaclass=abc.ABCMeta):
    def __init__(self, login, password, driver):
        self.login = login
        self.password = password
        self.driver = driver

    @abc.abstractmethod
    def logining(self):
        pass


class GetCourseNameAbs(metaclass=abc.ABCMeta):
    def __init__(self, driver):
        self.driver = driver

    @abc.abstractmethod
    def get_all_courses(self):
        pass


class SelectCourseAbs(metaclass=abc.ABCMeta):
    def __init__(self, course, driver):
        self.driver = driver
        self.course = self._course_validate(course)

    def _course_validate(self, course):
        if not isinstance(course, str):
            raise ValueError('tests must be str')
        return course

    @abc.abstractmethod
    def select_course(self):
        pass


class GetNameTestsAbs(metaclass=abc.ABCMeta):
    def __init__(self, driver):
        self.driver = driver

    @abc.abstractmethod
    def get_test_data(self):
        pass


class SelectTestAbs(metaclass=abc.ABCMeta):
    def __init__(self, tests, driver):
        self.driver = driver
        self.tests = self._test_validate(tests)

    def _test_validate(self, tests):
        if not isinstance(tests, list):
            raise ValueError('tests must be list')
        return tests

    def _get_current_url(self):
        self.current_url = self.driver.current_url

    @abc.abstractmethod
    def select_tests(self):
        pass

    @abc.abstractmethod
    def select_one_test(self, tests):
        pass

    @abc.abstractmethod
    def get_questions_and_answers(self):
        pass

    @abc.abstractmethod
    def _get_answers(self):
        pass

    @abc.abstractmethod
    def go_in_test(self):
        pass

    @abc.abstractmethod
    def mark_right_answer(self):
        pass

    @abc.abstractmethod
    def get_right_answer(self):
        pass
