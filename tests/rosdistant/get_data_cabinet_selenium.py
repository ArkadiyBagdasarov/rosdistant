import logging
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from rosdistant.core_rosdistant import LoginRosdistant, SelectCourse, GetCourseName, GetNameTests

logger = logging.getLogger()


def get_data_selenium(login_name, password):
    with GetCabinetData(login_name, password) as cabinet:
        cabinet.start()
    return cabinet.tests


class GetCabinetData:
    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()

        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)

        self.login_rosdistant = LoginRosdistant(self.login, self.password, self.driver)
        self.tests = {}

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.driver.close()
        self.driver.quit()
        self.display.stop()

    def start(self):
        logger.info('login in cabinet: {}'.format(self.login))
        self.login_rosdistant.logining()
        courses = self.get_course_name()
        logger.info('get course name: [ {} ]'.format(str(courses)))
        self.get_all_tests(courses)

    def get_course_name(self):
        courses_name = GetCourseName(self.driver)
        return courses_name.get_all_courses()

    def get_all_tests(self, courses):
        start_link = self.driver.current_url
        for course in courses:
            logger.info('START {}'.format(course))
            select_course = SelectCourse(course, self.driver)
            select_course.select_course()
            self.tests[course] = self.get_test_data()
            self.driver.get(start_link)
            logger.info('DONE {}'.format(course))

    def get_test_data(self):
        select_test = GetNameTests(self.driver)
        return select_test.get_test_data()
