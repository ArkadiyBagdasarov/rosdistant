import json

import logging
import os

from django.conf import settings
from selenium.common.exceptions import NoSuchElementException

from .abstract_class import LoginAbs, SelectCourseAbs, SelectTestAbs, GetCourseNameAbs, GetNameTestsAbs

logger = logging.getLogger()


class LoginRosdistant(LoginAbs):
    def logining(self):
        driver = self.driver
        driver.get("http://edu.rosdistant.ru")
        login = driver.find_element_by_id("login")
        login.send_keys(self.login)

        password = driver.find_element_by_id('password')
        password.send_keys(self.password)

        submit = driver.find_element_by_id('submit')
        submit.click()
        driver.implicitly_wait(5)


class GetCourseName(GetCourseNameAbs):
    def get_all_courses(self):
        courses = self.driver.find_elements_by_class_name('coursename')
        return [c.text for c in courses]


class SelectCourse(SelectCourseAbs):
    def select_course(self):
        driver = self.driver
        course = driver.find_element_by_link_text(self.course)
        course.click()
        driver.implicitly_wait(5)


class GetNameTests(GetNameTestsAbs):
    def get_test_data(self):
        tests = self.driver.find_elements_by_class_name('modtype_quiz')
        data = {
            test.find_element_by_class_name('instancename').text: test.find_element_by_class_name('gradesoncoursepage').text
            for test in tests
            if test.find_element_by_class_name('instancename').text.startswith('П')
        }
        return data


class SelectTest(SelectTestAbs):
    def select_tests(self):
        self._get_current_url()

        driver = self.driver
        self.title_course = driver.title
        for test in self.tests:
            logger.info('------ start {} ------'.format(test))
            self.select_one_test(test)
        driver.get(self.current_url)

    def select_one_test(self, test):
        driver = self.driver
        driver.find_element_by_link_text(test).click()
        driver.implicitly_wait(5)
        self.title = driver.title
        region_main = driver.find_element_by_id('region-main')
        try:
            button = region_main.find_elements_by_link_text('Просмотр')[-1]
        except (NoSuchElementException, IndexError):
            button = region_main.find_element_by_tag_name('input')
        button.click()
        driver.implicitly_wait(5)
        try:
            end_test = driver.find_element_by_link_text('Закончить попытку...')
            end_test.click()
            driver.implicitly_wait(5)
            get_result_form = driver.find_element_by_xpath('//*[@id="region-main"]/div/div[3]/div/div/form')
            get_result_button = get_result_form.find_element_by_tag_name('input')
            get_result_button.click()
            driver.implicitly_wait(5)
            confirm = driver.find_element_by_class_name('confirmation-buttons').find_element_by_tag_name('input')
            confirm.click()
            driver.implicitly_wait(5)
        except NoSuchElementException:
            pass
        driver.implicitly_wait(5)
        driver.find_element_by_link_text('Отображать все вопросы на одной странице').click()
        driver.implicitly_wait(5)

        self.get_questions_and_answers()

    def get_questions_and_answers(self):
        driver = self.driver
        all_q = driver.find_element_by_class_name('questionflagsaveform').find_elements_by_class_name(
            'deferredfeedback')
        for que in all_q:
            question = que.find_element_by_class_name('qtext')
            try:
                question_text = question.find_element_by_tag_name('p')
            except NoSuchElementException:
                question_text = question
            answer = que.find_element_by_class_name('rightanswer')
            q_text = question_text.text
            a_text_raw = answer.text
            import re
            res = re.search('(: )(.+)', a_text_raw)
            try:
                a_text = res.group(2)
            except AttributeError:
                continue
            dict_test = {
                'q': q_text,
                'a': a_text
            }
            home = os.path.join(settings.BASE_DIR + '/../../movie/texts/')
            with open(home + self.title_course + ' итоговый тест', 'a') as all_in_one:
                with open(home + self.title, 'a') as file:
                    file.write(json.dumps(dict_test, ensure_ascii=False))
                    file.write('\n')

                    all_in_one.write(json.dumps(dict_test, ensure_ascii=False))
                    all_in_one.write('\n')

        driver.find_element_by_link_text('Закончить обзор').click()
        driver.implicitly_wait(5)
        self.go_in_test()

    def _get_answers(self):
        self.answers = []
        home = os.path.join(settings.BASE_DIR + '/../../movie/texts/')
        with open(home + self.title, 'r') as f:
            for s in f.readlines():
                string = json.loads(s)
                self.answers.append(string)

    def go_in_test(self):
        driver = self.driver
        singlebutton = driver.find_element_by_class_name('singlebutton')
        button = singlebutton.find_element_by_tag_name('input')
        button.click()
        driver.implicitly_wait(5)
        count_iter = len(driver.find_element_by_class_name('qn_buttons').find_elements_by_tag_name('a'))
        self._get_answers()
        for i in range(count_iter):
            resp = self.mark_right_answer()
            if resp == 'end':
                break
        driver.get(self.current_url)

    def mark_right_answer(self):
        driver = self.driver
        try:
            right_answer = [self.get_right_answer()]
        except NoSuchElementException:
            right_answer = ['end']

        if right_answer[0] is not None and right_answer[0] != 'end':
            try:
                answer = driver.find_element_by_class_name('answer')
            except NoSuchElementException:
                driver.find_element_by_class_name('submitbtns').find_element_by_tag_name('input').click()
                driver.implicitly_wait(5)
                return False

            labels = answer.find_elements_by_tag_name('label')
            for label in labels:
                if label.text in right_answer:
                    label.click()
                    driver.implicitly_wait(5)
                else:
                    qs = [i.strip() for i in right_answer[0].split(',')]
                    if label.text in qs:
                        label.click()
                        driver.implicitly_wait(5)

        elif right_answer[0] == 'end':
            return 'end'

        driver.find_element_by_class_name('submitbtns').find_element_by_tag_name('input').click()

    def get_right_answer(self):
        driver = self.driver
        qtext = driver.find_element_by_class_name('qtext')
        try:
            question_text = qtext.find_element_by_tag_name('p')
        except NoSuchElementException:
            question_text = qtext
        q_text = question_text.text
        r_answer = None
        for answer in self.answers:
            if answer['q'] == q_text:
                r_answer = answer['a']
                break
        return r_answer
