import logging
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from .core_rosdistant import LoginRosdistant, SelectCourse, SelectTest

logger = logging.getLogger()


class DoTest:
    def __init__(self, login, password, course, tests):
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()

        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME)

        self.login_rosdistant = LoginRosdistant(login, password, self.driver)
        self.select_course = SelectCourse(course, self.driver)
        self.select_test = SelectTest(tests, self.driver)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.driver.close()
        self.driver.quit()
        self.display.stop()

    def start(self):
        logger.info('------ start login in cabinet ------')
        self.login_rosdistant.logining()

        logger.info(' ------ start select_course ------')
        self.select_course.select_course()

        logger.info(' ------ start select_tests ------')
        self.select_test.select_tests()
