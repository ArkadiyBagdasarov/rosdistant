from django.db import transaction


def get_user_account(user):
    if user is None:
        return None

    if hasattr(user, 'account'):
        return user.account
    else:
        return None


def change_user_email(user, new_email):
    user.email = user.username = new_email

    with transaction.atomic():
        user.save()


def change_account_phone(account, new_phone):
    account.phone = new_phone

    with transaction.atomic():
        account.save()
