from django.contrib.auth.models import User
from django.db import models
from django.db import transaction


class AccountManager(models.Manager):
    @classmethod
    def create_account_user(cls, email, password, phone, **kwargs):
        with transaction.atomic():
            user = User.objects.create_user(
                username=email,
                email=email,
                password=password
            )

            account = Account(
                user=user,
                phone=phone,
                **kwargs
            )

            account.save()

        return user


class Account(models.Model):
    objects = AccountManager()

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField('Номер телефона', max_length=11)
    first_name = models.CharField('Имя', max_length=128, null=True,
                                  blank=True)
    last_name = models.CharField('Фамилия', max_length=128, null=True,
                                 blank=True)
    middle_name = models.CharField('Отчество', max_length=128, null=True,
                                   blank=True)
    last_activity_time = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'

    def __str__(self):
        user_id, email = self.get_user_fields()

        return 'id={}; user_id={}; email={}'.format(
            self.id, user_id, email
        )

    def get_user_fields(self):
        try:
            user = self.user
            return user.id, user.email

        except Exception:
            return '-', '-'
